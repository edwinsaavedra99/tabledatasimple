document.addEventListener('DOMContentLoaded', init, false);

async function init() {

    // Select the table (well, tbody)
    let table = document.querySelector('#userTable tbody');
    // get the users
    let resp = await fetch('https://reqres.in/api/users?page=1');
    let data = await resp.json();
    // create html
    let result = '';
    data.data.forEach(c => {
        result += 
        `<tr>
            <td>
                <div class="img" style="background-image: url(${c.avatar});"></div>
            </td>
            <td>${c.first_name}</td>
            <td>${c.last_name}</td>
            <td>${c.email}</td>
            <td>
                <img src="./assets/icon-see-60.png" class="table-row__edit"/>
            </td>
        </tr>`;
    });
    table.innerHTML = result;
}