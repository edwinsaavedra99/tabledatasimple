
const templateCard = document.createElement('template');
templateCard.innerHTML = `
    <style>
    .modal-main {
        position:fixed;
        background: white;
        color: var(--main-color);
        width: 30%;
        padding: 2rem;
        height: auto;
        top:50%;
        transition: 0.3s ease-in-out;
        left:50%;
        transform: translate(-50%,-50%);
    }
    
    .modal-text{
        text-align: center;
    }
    .icon-modal{
        background: transparent;
        font-size: 2rem;
        cursor: pointer;
        outline: none;
        position: absolute;
        top: 1rem;
        right: 1rem;
    }
    .close-icon-modal{
        color: var(--main-color);
    }
    
    .img{
      
        width: 140px;
        height: 140px;
        border-radius: 50%;
        margin: 0 auto;
        background-size: cover;
        background-repeat: no-repeat;
        background-position: center center;
      }
    </style>

    <div class="showHideClassName">
        <section class="modal-main">
            <div class="img" style="background-image: url();"></div>
            <div class="modal-text">
                <p id="email"></p>
                <p id="first_name"></p>
                <p id="last_name"></p>
            </div>
            <div class="icon-modal">
                <img class="icon-modal-img" src="https://img.icons8.com/material-outlined/24/000000/delete-sign.png"/>
            </div>
        </section>
    </div>


    `

class UserCard extends HTMLElement {
  constructor() {
    super();
    this.avatar
    this.first_name;
    this.last_name;
    this.email;
    this.attachShadow({ mode: 'open' });
    this.shadowRoot.appendChild(templateCard.content.cloneNode(true));

  }


  connectedCallback() {
    /*
    this.shadowRoot.querySelector("#last_name").innerText = this.getAttribute("last_name");
    this.shadowRoot.querySelector("#email").innerText = this.getAttribute("email");*/

    this.shadowRoot.querySelector(".icon-modal-img").addEventListener('click', () => {
        this.dispatchEvent(new Event("close", { detail: true }));
      });
  }

  disconnectedCallback() {
    this.shadowRoot.querySelector(".icon-modal-img").removeEventListener('click');
  }

  static get observedAttributes() {
    return ['avatar', 'first_name', 'last_name', 'email'];
  }

  attributeChangedCallback(nameAtr, oldValue, newValue) {
    switch (nameAtr) {
      case "avatar":
        this.avatar = newValue;
        this.shadowRoot.querySelector(".img").style.backgroundImage = "url(" + this.getAttribute("avatar") + ")";
        break;
      case "first_name":
        this.first_name = newValue;
        this.shadowRoot.querySelector("#first_name").innerText = this.getAttribute("first_name");
        break;
      case "last_name":
        this.shadowRoot.querySelector("#last_name").innerText = this.getAttribute("last_name");
        this.last_name = newValue;
        break;
      case "email":
        this.shadowRoot.querySelector("#email").innerText = this.getAttribute("email");
        this.email = newValue;
        break;
    }
  }


}

window.customElements.define('user-modal-card', UserCard);
